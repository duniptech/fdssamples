package ent;
import java.util.logging.*;
import xdevs.core.*;
import xdevs.core.util.*;
import java.util.*;
import xdevs.core.modeling.*;


public class Start extends Entity{

	
	public Start(){
		this("Start");	
	}
	
	public Start(String name){
		super(name);
	}
	
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(getName()+"{");
		sb.append("}");
		return sb.toString();
	}
}
