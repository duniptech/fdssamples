package ent;
import java.util.logging.*;
import xdevs.core.*;
import xdevs.core.util.*;
import java.util.*;
import xdevs.core.modeling.*;


public class Result extends Entity{

	protected double thruput;
	protected int arrived;
	protected int solved;
	protected int lastSolved;
	
	public Result(){
		this("Result");	
	}
	
	public Result(String name){
		super(name);
	}
	
	public double getThruput(){
		return thruput;
	}
	
	public void setThruput(double value){
		this.thruput = value;
	}
	public int getArrived(){
		return arrived;
	}
	
	public void setArrived(int value){
		this.arrived = value;
	}
	public int getSolved(){
		return solved;
	}
	
	public void setSolved(int value){
		this.solved = value;
	}
	public int getLastSolved(){
		return lastSolved;
	}
	
	public void setLastSolved(int value){
		this.lastSolved = value;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(getName()+"{");
		sb.append("(thruput:"+String.format("%.4f", thruput)+")");
		sb.append("(arrived:"+arrived+")");
		sb.append("(solved:"+solved+")");
		sb.append("(lastSolved:"+lastSolved+")");
		sb.append("}");
		return sb.toString();
	}
}
