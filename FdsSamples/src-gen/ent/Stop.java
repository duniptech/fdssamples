package ent;
import java.util.logging.*;
import xdevs.core.*;
import xdevs.core.util.*;
import java.util.*;
import xdevs.core.modeling.*;


public class Stop extends Entity{

	
	public Stop(){
		this("Stop");	
	}
	
	public Stop(String name){
		super(name);
	}
	
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(getName()+"{");
		sb.append("}");
		return sb.toString();
	}
}
