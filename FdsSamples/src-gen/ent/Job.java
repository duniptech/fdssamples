package ent;
import java.util.logging.*;
import xdevs.core.*;
import xdevs.core.util.*;
import java.util.*;
import xdevs.core.modeling.*;


public class Job extends Entity{

	protected int id;
	protected double solvedAt;
	
	public Job(){
		this("Job");	
	}
	
	public Job(String name){
		super(name);
	}
	
	public int getId(){
		return id;
	}
	
	public void setId(int value){
		this.id = value;
	}
	public double getSolvedAt(){
		return solvedAt;
	}
	
	public void setSolvedAt(double value){
		this.solvedAt = value;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(getName()+"{");
		sb.append("(id:"+id+")");
		sb.append("(solvedAt:"+String.format("%.4f", solvedAt)+")");
		sb.append("}");
		return sb.toString();
	}
}
