package gpCoupled;
import gpAtomics.Genr;
import java.util.logging.*;
import ent.Stop;
import ent.Start;
import gpAtomics.Transd;
import gpAtomics.Proc;
import xdevs.core.*;
import xdevs.core.util.*;
import xdevs.core.simulation.*;
import java.util.*;
import xdevs.core.modeling.*;
import xdevs.core.simulation.trace.*;
import ent.Result;

public class gpt extends Coupled{

	private static final Logger logger = Logger.getLogger(gpt.class.getName());
	public InPort<Start> stc = new InPort<>("stc");
	public InPort<Stop> spc = new InPort<>("spc");
	public OutPort<Result> rsc = new OutPort<>("rsc");
	
	public static void main(String... args){
		DevsLogger.setup(Level.FINEST); //Levels available INFO -> FINE -> FINER -> FINEST -> ALL
		TraceCoordinator c = new TraceCoordinator(new gpt()); //with auto-generated Sequence diagram trace
		//Coordinator c = new Coordinator(new gpt()); //fast-mode with no trace
		c.initialize();
		c.simulate(Long.MAX_VALUE);
	}
	
	public gpt(){
		this("gpCoupled.gpt", false);
	}
	
	public gpt(boolean debug){
		this("gpCoupled.gpt", debug);
	}
	
	public gpt(String name, boolean debug){
		super(name);
		
		addInPort(stc);
		addInPort(spc);
		addOutPort(rsc);
		
		Genr g = new Genr("GpCoupled.gpt.g", debug);
		addComponent(g);
		
		Proc p = new Proc("GpCoupled.gpt.p", debug);
		addComponent(p);
		
		Transd t = new Transd("GpCoupled.gpt.t", debug);
		addComponent(t);
		
		
		addCoupling(this.stc,g.st);	
		
		addCoupling(this.spc,g.sp);	
		
		addCoupling(g.job,p.jb);
		
		addCoupling(g.job,t.arrived);
		
		addCoupling(p.rs,t.solved);
		
		addCoupling(t.stop,g.sp);
		
		addCoupling(t.res,this.rsc);
	}
}		
	
