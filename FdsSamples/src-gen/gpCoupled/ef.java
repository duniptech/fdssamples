package gpCoupled;
import gpAtomics.Genr;
import java.util.logging.*;
import xdevs.core.*;
import xdevs.core.util.*;
import xdevs.core.simulation.*;
import ent.Start;
import java.util.*;
import gpAtomics.Transd;
import xdevs.core.modeling.*;
import ent.Job;
import xdevs.core.simulation.trace.*;
import ent.Result;

public class ef extends Coupled{

	private static final Logger logger = Logger.getLogger(ef.class.getName());
	public InPort<Start> stc = new InPort<>("stc");
	public InPort<Job> jobSol = new InPort<>("jobSol");
	public OutPort<Job> jobGen = new OutPort<>("jobGen");
	public OutPort<Result> rsc = new OutPort<>("rsc");
	
	public static void main(String... args){
		DevsLogger.setup(Level.FINEST); //Levels available INFO -> FINE -> FINER -> FINEST -> ALL
		TraceCoordinator c = new TraceCoordinator(new ef()); //with auto-generated Sequence diagram trace
		//Coordinator c = new Coordinator(new ef()); //fast-mode with no trace
		c.initialize();
		c.simulate(Long.MAX_VALUE);
	}
	
	public ef(){
		this("gpCoupled.ef", false);
	}
	
	public ef(boolean debug){
		this("gpCoupled.ef", debug);
	}
	
	public ef(String name, boolean debug){
		super(name);
		
		addInPort(stc);
		addInPort(jobSol);
		addOutPort(jobGen);
		addOutPort(rsc);
		
		Genr g = new Genr("GpCoupled.ef.g", debug);
		addComponent(g);
		
		Transd t = new Transd("GpCoupled.ef.t", debug);
		addComponent(t);
		
		
		addCoupling(g.job,t.solved);
	}
}		
	
