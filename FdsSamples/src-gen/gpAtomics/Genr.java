
//1. specify package
package gpAtomics;

//2. add DEVS modeling imports for specific engineType
import java.util.logging.*;
import ent.Stop;
import xdevs.core.*;
import xdevs.core.util.*;
import ent.Start;
import java.util.*;
import xdevs.core.modeling.*;
import ent.Job;

//3. create class and extend from DEVS atomic{
public class Genr extends Atomic{
	
	private boolean debug = false;	
	//4. define variables
	protected double arrivTime;
	protected int count;

	//5. define empty hashtable for State-time-advances
	private Hashtable<String,Double> sta;
	private double clock = 0.0;
		
	//6. define state strings
	private final String PASSIVE = "passive";
	private final String ACTIVE = "active";
	private final String FINISHING = "finishing";
	
	private static final Logger logger = Logger.getLogger(Genr.class.getName());
	
	public InPort<Start> st = new InPort<>("st");
	public InPort<Stop> sp = new InPort<>("sp");
	public OutPort<Job> job = new OutPort<>("job");
		
	//7. Create constructor with name
	public Genr(String name){
		super(name);
		addInPort(st);
		addInPort(sp);
		addOutPort(job);
					
		//8. initialize hashtable for state-time-advances
		sta = new Hashtable<String, Double>();
				
		//unpack initialization code
		arrivTime = 1.0; 
						count = 0;
						;
		
		//unpack state-time-advances
		sta.put(PASSIVE,Constants.INFINITY);
		sta.put(ACTIVE,  arrivTime);
		sta.put(FINISHING,  0.0);
	}
		
	public Genr(String name, boolean debug){
		this(name);
		this.debug = debug;
	}
	
	@Override
	public void initialize(){	
		//hold in initial state
		holdIn(ACTIVE,sta.get(ACTIVE));
		clock = 0.0;
		logger.finer(showState());
	}			
			
	//9. Internation Transition Function
	@Override
	public void deltint(){
		clock += getSigma();
		//unpack AtomicBeh.deltint
		if(phaseIs(ACTIVE)){
			if(processDeltintActiveActive()){
				holdIn(ACTIVE,sta.get(ACTIVE));
			} else {
				showError("Processing instruction failed during internal transition. Going to fall-back state!!");
				passivate();
			}
		}
		//unpack AtomicBeh.deltint
		else
		if(phaseIs(FINISHING)){
			if(processDeltintFinishingPassive()){
				holdIn(PASSIVE,sta.get(PASSIVE));
			} else {
				showError("Processing instruction failed during internal transition. Going to fall-back state!!");
				passivate();
			}
		}
		else
		passivate();
		
		logger.finer(showState());
	}
		
	//10. External Transition Function
	@Override
	public void deltext(double e){
		//advance elapsed time
		clock += e;
		if(phaseIs(ACTIVE)){
			boolean processInput = false;
			if(!sp.isEmpty()){
				Stop spInstance = sp.getSingleValue();	
				showInput(spInstance);
				processInput = processDeltextActiveSp(spInstance);						
				if(processInput){
					holdIn(FINISHING, sta.get(FINISHING));
				} else{
					showError("Input processing returned false. Passivating!!!");
					//no state change. Stay in same state for sigma
				}
			}
		}
		else
		if(phaseIs(PASSIVE)){
			boolean processInput = false;
			if(!st.isEmpty()){
				Start stInstance = st.getSingleValue();	
				showInput(stInstance);
				processInput = processDeltextPassiveSt(stInstance);						
				if(processInput){
					holdIn(ACTIVE, sta.get(ACTIVE));
				} else{
					showError("Input processing returned false. Passivating!!!");
					//no state change. Stay in same state for sigma
				}
			}
		}
		
	
		logger.finer(showState());
	}
				
	//11. Confluent Function	
	@Override
	public void deltcon(double e){
		
		//putting default AtomicBeh.confluent
		deltint();
		deltext(e);
		
		logger.finer(showState());
	}			
				
	//12. Output Function
	@Override
	public void lambda(){
		//unpack AtomicBeh.outfn
		if(phaseIs(ACTIVE)){
			Job jobInstance = new Job();
			processOutActiveJob(jobInstance);
			job.addValue(jobInstance);
			showOutput(jobInstance);
		}
	}
			
	public void showOutput(Entity en){
		logger.finest((clock+sigma)+"\t\t"+getName()+"\t\tOUTPUT:"+en);
	}
	
	public void showInput(Entity en){
		logger.finest(clock + "\t\t"+getName()+"\t\tINPUT:"+en);
	}
	
	public void showError(String en){
		logger.severe(clock + "\t\t"+getName()+"\t\t"+en);
	}
	
	public String showState(){
		return clock + "\t\t"+getName()+ "\t\tSTATE:"+getPhase()+",SIGMA:"+getSigma();
	}
			
	//14. write processing function
	
	private boolean processDeltintActiveActive(){
		boolean returnVal = true;
		count++;
					;
		return returnVal;
	}
	
	private boolean processDeltintFinishingPassive(){
		boolean returnVal = true;
		return returnVal;
	}
	
	private boolean processDeltextActiveSp(Stop sp){
		boolean returnVal = true;
		return returnVal;
	}
	
	private boolean processDeltextPassiveSt(Start st){
		boolean returnVal = true;
		return returnVal;
	}
	
	private boolean processOutActiveJob(Job job){
		boolean returnVal = true;
		job.setId(count);;
		return returnVal;
	}
	
	//15. End class 
}

