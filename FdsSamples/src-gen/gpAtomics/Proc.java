
//1. specify package
package gpAtomics;

//2. add DEVS modeling imports for specific engineType
import java.util.logging.*;
import xdevs.core.*;
import xdevs.core.util.*;
import java.util.*;
import xdevs.core.modeling.*;
import ent.Job;

//3. create class and extend from DEVS atomic{
public class Proc extends Atomic{
	
	private boolean debug = false;	
	//4. define variables
	protected double procTime;
	protected Job job;

	//5. define empty hashtable for State-time-advances
	private Hashtable<String,Double> sta;
	private double clock = 0.0;
		
	//6. define state strings
	private final String PASSIVE = "passive";
	private final String BUSY = "busy";
	
	private static final Logger logger = Logger.getLogger(Proc.class.getName());
	
	public InPort<Job> jb = new InPort<>("jb");
	public OutPort<Job> rs = new OutPort<>("rs");
		
	//7. Create constructor with name
	public Proc(String name){
		super(name);
		addInPort(jb);
		addOutPort(rs);
					
		//8. initialize hashtable for state-time-advances
		sta = new Hashtable<String, Double>();
				
		//unpack initialization code
		procTime = 3.0;;
		
		//unpack state-time-advances
		sta.put(PASSIVE,Constants.INFINITY);
		sta.put(BUSY,  procTime);
	}
		
	public Proc(String name, boolean debug){
		this(name);
		this.debug = debug;
	}
	
	@Override
	public void initialize(){	
		//hold in initial state
		holdIn(PASSIVE,sta.get(PASSIVE));
		clock = 0.0;
		logger.finer(showState());
	}			
			
	//9. Internation Transition Function
	@Override
	public void deltint(){
		clock += getSigma();
		//unpack AtomicBeh.deltint
		if(phaseIs(BUSY)){
			if(processDeltintBusyPassive()){
				holdIn(PASSIVE,sta.get(PASSIVE));
			} else {
				showError("Processing instruction failed during internal transition. Going to fall-back state!!");
				passivate();
			}
		}
		else
		passivate();
		
		logger.finer(showState());
	}
		
	//10. External Transition Function
	@Override
	public void deltext(double e){
		//advance elapsed time
		clock += e;
		if(phaseIs(PASSIVE)){
			boolean processInput = false;
			if(!jb.isEmpty()){
				Job jbInstance = jb.getSingleValue();	
				showInput(jbInstance);
				processInput = processDeltextPassiveJb(jbInstance);						
				if(processInput){
					holdIn(BUSY, sta.get(BUSY));
				} else{
					showError("Input processing returned false. Passivating!!!");
					//no state change. Stay in same state for sigma
				}
			}
		}
		
	
		logger.finer(showState());
	}
				
	//11. Confluent Function	
	@Override
	public void deltcon(double e){
		
		//putting default AtomicBeh.confluent
		deltint();
		deltext(e);
		
		logger.finer(showState());
	}			
				
	//12. Output Function
	@Override
	public void lambda(){
		//unpack AtomicBeh.outfn
		if(phaseIs(BUSY)){
			Job rsInstance = new Job();
			processOutBusyRs(rsInstance);
			rs.addValue(rsInstance);
			showOutput(rsInstance);
		}
	}
			
	public void showOutput(Entity en){
		logger.finest((clock+sigma)+"\t\t"+getName()+"\t\tOUTPUT:"+en);
	}
	
	public void showInput(Entity en){
		logger.finest(clock + "\t\t"+getName()+"\t\tINPUT:"+en);
	}
	
	public void showError(String en){
		logger.severe(clock + "\t\t"+getName()+"\t\t"+en);
	}
	
	public String showState(){
		return clock + "\t\t"+getName()+ "\t\tSTATE:"+getPhase()+",SIGMA:"+getSigma();
	}
			
	//14. write processing function
	
	private boolean processDeltintBusyPassive(){
		boolean returnVal = true;
		return returnVal;
	}
	
	private boolean processDeltextPassiveJb(Job jb){
		boolean returnVal = true;
		job = jb;
					;
		return returnVal;
	}
	
	private boolean processOutBusyRs(Job rs){
		boolean returnVal = true;
		rs.setId(job.getId());
					rs.setSolvedAt(clock);
					;
		return returnVal;
	}
	
	//15. End class 
}

