
//1. specify package
package gpAtomics;

//2. add DEVS modeling imports for specific engineType
import java.util.logging.*;
import ent.Stop;
import xdevs.core.*;
import xdevs.core.util.*;
import java.util.*;
import xdevs.core.modeling.*;
import ent.Job;
import ent.Result;

//3. create class and extend from DEVS atomic{
public class Transd extends Atomic{
	
	private boolean debug = false;	
	//4. define variables
	protected int arrivedCnt;
	protected int solvedCnt;
	protected double observeTime;
	protected int jobId;

	//5. define empty hashtable for State-time-advances
	private Hashtable<String,Double> sta;
	private double clock = 0.0;
		
	//6. define state strings
	private final String PASSIVE = "passive";
	private final String ACTIVE = "active";
	private final String DONE = "done";
	
	private static final Logger logger = Logger.getLogger(Transd.class.getName());
	
	public InPort<Job> arrived = new InPort<>("arrived");
	public InPort<Job> solved = new InPort<>("solved");
	public OutPort<Result> res = new OutPort<>("res");
	public OutPort<Stop> stop = new OutPort<>("stop");
		
	//7. Create constructor with name
	public Transd(String name){
		super(name);
		addInPort(arrived);
		addInPort(solved);
		addOutPort(res);
		addOutPort(stop);
					
		//8. initialize hashtable for state-time-advances
		sta = new Hashtable<String, Double>();
				
		//unpack initialization code
		observeTime = 100.0;;
		
		//unpack state-time-advances
		sta.put(PASSIVE,Constants.INFINITY);
		sta.put(ACTIVE,  observeTime);
		sta.put(DONE,  0.0);
	}
		
	public Transd(String name, boolean debug){
		this(name);
		this.debug = debug;
	}
	
	@Override
	public void initialize(){	
		//hold in initial state
		holdIn(ACTIVE,sta.get(ACTIVE));
		clock = 0.0;
		logger.finer(showState());
	}			
			
	//9. Internation Transition Function
	@Override
	public void deltint(){
		clock += getSigma();
		//unpack AtomicBeh.deltint
		if(phaseIs(ACTIVE)){
			if(processDeltintActiveDone()){
				holdIn(DONE,sta.get(DONE));
			} else {
				showError("Processing instruction failed during internal transition. Going to fall-back state!!");
				passivate();
			}
		}
		//unpack AtomicBeh.deltint
		else
		if(phaseIs(DONE)){
			if(processDeltintDonePassive()){
				holdIn(PASSIVE,sta.get(PASSIVE));
			} else {
				showError("Processing instruction failed during internal transition. Going to fall-back state!!");
				passivate();
			}
		}
		else
		passivate();
		
		logger.finer(showState());
	}
		
	//10. External Transition Function
	@Override
	public void deltext(double e){
		//advance elapsed time
		clock += e;
		if(phaseIs(ACTIVE)){
			boolean processInput = false;
			if(!arrived.isEmpty()){
				Job arrivedInstance = arrived.getSingleValue();	
				showInput(arrivedInstance);
				processInput = processDeltextActiveArrived(arrivedInstance);						
				if(processInput){
					holdIn(ACTIVE, sigma);
				} else{
					showError("Input processing returned false. Passivating!!!");
					//no state change. Stay in same state for sigma
				}
			}
			if(!solved.isEmpty()){
				Job solvedInstance = solved.getSingleValue();	
				showInput(solvedInstance);
				processInput = processDeltextActiveSolved(solvedInstance);						
				if(processInput){
					holdIn(ACTIVE, sigma);
				} else{
					showError("Input processing returned false. Passivating!!!");
					//no state change. Stay in same state for sigma
				}
			}
		}
		
	
		logger.finer(showState());
	}
				
	//11. Confluent Function	
	@Override
	public void deltcon(double e){
		//unpack AtomicBeh.confluent
		deltext(e);
		deltint();
		
		
		logger.finer(showState());
	}			
				
	//12. Output Function
	@Override
	public void lambda(){
		//unpack AtomicBeh.outfn
		if(phaseIs(DONE)){
			Result resInstance = new Result();
			Stop stopInstance = new Stop();
			processOutDoneResStop(resInstance, stopInstance);
			res.addValue(resInstance);
			showOutput(resInstance);
			stop.addValue(stopInstance);
			showOutput(stopInstance);
		}
	}
			
	public void showOutput(Entity en){
		logger.finest((clock+sigma)+"\t\t"+getName()+"\t\tOUTPUT:"+en);
	}
	
	public void showInput(Entity en){
		logger.finest(clock + "\t\t"+getName()+"\t\tINPUT:"+en);
	}
	
	public void showError(String en){
		logger.severe(clock + "\t\t"+getName()+"\t\t"+en);
	}
	
	public String showState(){
		return clock + "\t\t"+getName()+ "\t\tSTATE:"+getPhase()+",SIGMA:"+getSigma();
	}
			
	//14. write processing function
	
	private boolean processDeltintActiveDone(){
		boolean returnVal = true;
		return returnVal;
	}
	
	private boolean processDeltintDonePassive(){
		boolean returnVal = true;
		return returnVal;
	}
	
	private boolean processDeltextActiveArrived(Job arrived){
		boolean returnVal = true;
		
					arrivedCnt++;
					;
		return returnVal;
	}
	
	private boolean processDeltextActiveSolved(Job solved){
		boolean returnVal = true;
		
					solvedCnt++;
					jobId = solved.getId();
					;
		return returnVal;
	}
	
	private boolean processOutDoneResStop(Result res, Stop stop){
		boolean returnVal = true;
		
					res.setArrived(arrivedCnt);
					res.setSolved(solvedCnt);
					res.setThruput(1.0*solvedCnt/arrivedCnt);
					res.setLastSolved(jobId);
					;
		return returnVal;
	}
	
	//15. End class 
}

