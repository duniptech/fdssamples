package efCoupled;
import java.util.logging.*;
import gpAtomics.Proc;
import xdevs.core.*;
import xdevs.core.util.*;
import xdevs.core.simulation.*;
import java.util.*;
import xdevs.core.modeling.*;
import xdevs.core.simulation.trace.*;
import efCoupled.ef;

public class efp extends Coupled{

	private static final Logger logger = Logger.getLogger(efp.class.getName());
	
	public static void main(String... args){
		DevsLogger.setup(Level.FINEST); //Levels available INFO -> FINE -> FINER -> FINEST -> ALL
		TraceCoordinator c = new TraceCoordinator(new efp()); //with auto-generated Sequence diagram trace
		//Coordinator c = new Coordinator(new efp()); //fast-mode with no trace
		c.initialize();
		c.simulate(Long.MAX_VALUE);
	}
	
	public efp(){
		this("efCoupled.efp", false);
	}
	
	public efp(boolean debug){
		this("efCoupled.efp", debug);
	}
	
	public efp(String name, boolean debug){
		super(name);
		
		
		ef EF = new ef("EfCoupled.efp.EF", debug);
		addComponent(EF);
		
		Proc p = new Proc("EfCoupled.efp.p", debug);
		addComponent(p);
		
		
		addCoupling(EF.jobGen,p.jb);
		
		addCoupling(p.rs,EF.jobSol);
	}
}		
	
